package todo

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"fmt"
	"time"
)

//  Create a struct for the items
type item struct {
	//  Set the required details
	Task 		string
	Done 		bool
	CreatedAt 	time.Time
	CompletedAt time.Time
}

type Todos []item

//  The function takes todos values and
//  a task to add onto the list
func (t *Todos) Add(task string) {

	//  Sets the structure of the task
	todo := item{
		Task: 			task,
		Done: 			false,
		CreatedAt: 		time.Now(),
		CompletedAt: 	time.Time{},
	}

	//  Append the task to the todo
	*t = append(*t, todo)

}

//  The function takes the todos values and
//  an index to mark as complete
func (t *Todos) Complete(index int) error {
	//  Create a copy of the todos
	ls := *t

	//  If the index is out of bound...
	if index <= 0 || index > len(ls) {
		//  Return an error
		return errors.New("Invalid index")
	}

	//  Otherwise, mark the completion time
	ls[index-1].CompletedAt = time.Now()
	//  Mark the task as complete
	ls[index-1].Done = true

	//  Return no error
	return nil
}

func (t *Todos) Delete(index int) error {
	ls := *t

	if index <= 0 || index > len(ls) {
		return errors.New("Invalid index")
	}

	//  Set the list of tasks to everything
	//  except the specified index
	*t = append(ls[:index-1], ls[index:]...)

	return nil
	
}

//  Function to load a todo
func (t *Todos) Load(filename string) error {
	//  Read the file and store it and any
	//  potential error
	file, err := ioutil.ReadFile(filename)

	//  Do some error checking
	if err != nil {
		//  If the file doesn't exist (has no todos)
		if errors.Is(err, os.ErrNotExist) {
			//  ...Return nil because its not an error
			return nil
		}
		return err
	}

	//  If the file is empty
	if len(file) == 0 {
		return err
	}

	//  Unmarshal the data (decode it)
	//  i.e {"name":john} vs name: john
	err = json.Unmarshal(file, t)
	if err != nil {
		return err
	}

	return nil
}

func (t *Todos) Store(filename string) error {

	//  Marshal the data (encode it)
	data, err := json.Marshal(t)

	//  Error checking
	if err != nil {
		return err
	}

	//  Write the filename, data and the permissions
	return ioutil.WriteFile(filename, data, 0644)
}

func (t *Todos) Print() {

	//  For each item in the list
	for i, item := range *t {
		//  Print the index and item
		fmt.Printf("%d - %s\n",i, item.Task)
	}
}
