package main

import (
	"flag"
	"fmt"
	"github.com/dexterscodinglab/golang-todo"
	"strings"
	"bufio"
	"io"
	"os"
	"errors"
)

const (
	//  Create a hidden todos file
	todoFile = ".todos.json"
)

func main() {

	//  Create flags (name, value and description)
	add := flag.Bool("add", false, "adds a new todo")
	complete := flag.Int("complete", 0, "marks a todo as completed")
	del := flag.Int("del", 0, "deletes a todo")
	list := flag.Bool("list", false, "displays the list of todos")

	//  Read the flags
	flag.Parse()

	//  Take the Todos structure created in the "todos" package
	todos := &todo.Todos{}

	if err := todos.Load(todoFile); err != nil {
		//  Write error to stderr
		fmt.Fprintln(os.Stderr, err.Error())
		//  Exit with exit code 1
		os.Exit(1)
	}

	switch{
		//  Add a todo...
		case *add:
			task, err := getInput(os.Stdin, flag.Args()...)
			if err != nil {
				fmt.Fprintln(os.Stderr, err.Error())
				os.Exit(1)
			}

			todos.Add(task)
			
			err = todos.Store(todoFile)
			if err != nil {
				fmt.Fprintln(os.Stderr, err.Error())
				os.Exit(1)
			}		

		//  Mark a todo as complete...
		case *complete > 0:
			//  Mark the value (index) of *complete as
			//  completed (marks a todo as complete)
			err := todos.Complete(*complete)
			if err != nil {
				fmt.Fprintln(os.Stderr, err.Error())
				os.Exit(1)
			}

			//  Store the completed todo
			err = todos.Store(todoFile)
			if err != nil {
				fmt.Fprintln(os.Stderr, err.Error())
				os.Exit(1)
			}
			
		//  Deleting a todo...
		case *del > 0:
			err := todos.Delete(*del)
			if err != nil {
				fmt.Fprintln(os.Stderr, err.Error())
				os.Exit(1)
			}

			//  Store the completed todo
			err = todos.Store(todoFile)
			if err != nil {
				fmt.Fprintln(os.Stderr, err.Error())
				os.Exit(1)
			}
		//  Listing all todos...
		case *list:
			todos.Print()
		default:
			fmt.Fprintln(os.Stdout, "invalid command")
			os.Exit(0)
	}

}

func getInput(r io.Reader, args ...string) (string, error) {

	//  If the user types i.e -add string1 string2
	if len(args) > 0 {
		//  Join the strings that are separated
		//  by spaces
		return strings.Join(args, " "), nil
	}

	//  Create a new scanner object
	scanner := bufio.NewScanner(r)
	//  Scan the input
	scanner.Scan()

	//  Error checking
	if err := scanner.Err(); err != nil {
		return "", err
	}

	//  Store the inputted text
	text := scanner.Text()

	//  If the text is empty
	if len(text) == 0 {
		//  Return error
		return "", errors.New("Empty todo is not allowed.")
	}

	//  Otherwise return the text
	return scanner.Text(), nil
	
}
